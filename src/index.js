import React from 'react';
import ReactDOM from 'react-dom';
import Login from './components/login/login';
import 'react-notifications/lib/notifications.css';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import Promise from 'promise-polyfill'
if (!window.Promise) {
  window.Promise = Promise;
}

// import '../css/right.dark.css'
import '../css/demo.css'

// import '../libs/bootstrap/css/bootstrap.min.css'
import '../libs/jquery.scrollbar/jquery.scrollbar.css'
import '../libs/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css'

Object.assign = require('object-assign');
require('babel-polyfill');


ReactDOM.render(
  <div>

    <Login />
    <NotificationContainer />

  </div>,
  document.getElementById('app')
);

// module.hot.accept();
