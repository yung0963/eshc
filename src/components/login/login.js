import React from 'react';
import { GoogleLogin } from 'react-google-login';
import Templet from '../templet/templet'
import Dashboard from '../templet/dashboard'
import Back_Templet from '../back/back_templet'
import Back_Dashboard from '../back/back_dashboard'
import { getCookie, setCookie } from '../../model/function'
const { Map } = require('immutable')
import logo from '../../../img/rightlogo.png'
import logoicon from '../../../img/icon.png'
import { NotificationManager } from 'react-notifications';
import 'isomorphic-fetch';
import 'react-notifications/lib/notifications.css';


export default class Login extends React.Component {
  constructor(props) {
    super(props);


    let userInfo = getCookie("userInfo");
    let cookieUserInfo = null;

    if (Object.keys(userInfo).length > 0 && userInfo != "null") {
      cookieUserInfo = JSON.parse(userInfo)

    } else {
      console.log("cookie is expired")
    }


    let userBase = getCookie("userBase");
    let cookieuserBase = null

    if (Object.keys(userBase).length > 0 && userBase != "null") {
      cookieuserBase = JSON.parse(userBase)
    }
    this.state = {
      userInfo: cookieUserInfo || null,
      userBase: cookieuserBase || {
        mybase: null,
        manger: null


      },
      page: "student",
      url: "http://eshc.osha.web.nkust.edu.tw/api/"
      // url: "http://localhost/capi/api/"

    }

    this.responseGoogle = this.responseGoogle.bind(this);
    this.logout = this.logout.bind(this);
    this.changeMyPage = this.changeMyPage.bind(this);
    this.changuserInfo = this.changuserInfo.bind(this)

  }
  changeMyPage($pag) {

    this.setState({ page: $pag });
  }

  changuserInfo() {
    fetch(this.state.url + "login",
      {
        method: 'post',
        body: JSON.stringify(this.state.userInfo)
      }).then(response => Promise.all([response, response.json()])).then(([response, json]) => {
        if (response.status === 200) {

          if (json.status == "success") {

            setCookie("userBase", JSON.stringify(json), 10000)

            this.setState({ userBase: json })

          }
        }
        else {

        }
      }).catch((error) => {
        console.log(error)
      });

  }
  responseGoogle(params) {


    fetch(this.state.url + "login",
      {
        method: 'post',
        body: JSON.stringify(params.profileObj)
      }).then(response => Promise.all([response, response.json()])).then(([response, json]) => {
        if (response.status === 200) {

          switch (json.status) {
            case "success":
              setCookie("userInfo", JSON.stringify(params.profileObj), 10000)
              setCookie("userBase", JSON.stringify(json), 10000)

              this.setState({ userInfo: params.profileObj, userBase: json })
              NotificationManager.info(json.msg);
              break;
            case "assis":
              setCookie("userInfo", JSON.stringify(params.profileObj), 10000)
              setCookie("userBase", JSON.stringify(json), 10000)

              this.setState({ userInfo: params.profileObj, userBase: json })
              NotificationManager.info(json.msg);
              break;
            default:
              NotificationManager.warning(json.msg, 'warning', 3000);
              break

          }

        }
        else {

        }
      }).catch((error) => {
        console.log(error)
      });



  }

  logout() {


    let js = { manger: null }
    this.setState({ userBase: js, userInfo: null });

    setCookie("userInfo", "null", 1)
    setCookie("userBase", "null", 1)


  }

  componentDidMount() {



  }
  render() {


    // 判斷有沒有登入

    if (this.state.userInfo != null) {
      if (this.state.userBase.manger == this.state.userInfo.email && this.state.userBase.manger != null) {

        return (
          <div className="wrapper">
            <Back_Templet user_data={this.state.userInfo} user_base={this.state.userBase} action={this.logout} page={this.state.page} changeMyPage={this.changeMyPage} url={this.state.url} />

            <Back_Dashboard user_data={this.state.userInfo} user_base={this.state.userBase} page={this.state.page} changeMyPage={this.changeMyPage} url={this.state.url} />

          </div>
        )




      } else {
        return (
          <div className="wrapper">

            <Templet user_data={this.state.userInfo} user_base={this.state.userBase} action={this.logout} url={this.state.url} />
            <Dashboard user_data={this.state.userInfo} user_base={this.state.userBase} url={this.state.url} changuserInfo={this.changuserInfo} />

          </div>
        )
      }
    } else {
      return (

        <div className="wrapper_login" style={{ width: '60%', left: '20%', top: '5%' }}>

          <div className="login" >
            <form className="login__form" action="index.html">
              <div className="login__logo">
                <img src={logo} width="450px" />
              </div>


              <h3 style={{ textAlign: "center", color: "white", background: "rgb(242,168,19)", marginTop: "-20px", lineHeight: "60px", fontWeight: "900", letterSpacing: "5px", height: "80px", paddingTop: "10px" }}><img width="50px" src={logoicon} /> 實驗(習)場所安全衛生教育宣導</h3>
              <div style={{ paddingLeft: "20px", color: "#565656" }}>
                <h4 style={{ fontWeight: "900" }}>實施目的</h4>
                <span style={{}}>為保障本校教職員生之安全與健康，建構實驗(習)場所安全學習環境，實施新進學生入場(廠)安全學習教育宣導。</span><br />

                <h4 style={{ fontWeight: "900" }}>實施日期</h4>
                <span style={{}}>自即日起至107年9月20日止。</span><br />

                <h4 style={{ fontWeight: "900" }}>實施對象</h4>
                <ul style={{}}>
                  <li>建工/燕巢校區</li>
                  工學院：化材系、工管系、土木系、機械系、模具系<br />
                  電資學院：光通所、電機系、電子系、資工系<br />
                  管理學院：觀光系<br />
                  人文社會學院：文創系

                    <li>第一校區</li>
                  工學院：營建系、環安系、機械系、創設系<br />
                  電資學院：電機所、電通系、電子系

                    <li>楠梓/旗津校區</li>
                  海工學院：造船系、電訊系、微電系、海環系<br />
                  水圈學院：養殖系、水食系、海生系、漁管系<br />
                  海事學院：航技系、輪機系、海資系

                </ul>
                <br />
                <span style={{}}>上開學院所屬之系所，學制為專科、四技、 二技、碩博士(含在職專班)新生皆需接受本次訓練。</span>
                <br />
                <h4 style={{ fontWeight: "900" }}>實施方法</h4>
                <ul style={{}}>

                  <li> 採線上訓練方式，新進學生須完成本次訓練，始得進入實驗(習)場所；若未觀看完整影片（包含跳躍、加速或離開頁面），不得進入實驗(習)場所。</li>
                  <li>實施期間結束後，將彙整統計各單位參訓人員陳報備查；未完成本訓練之新生，將名單送交各單位主管，並請轉知所屬。
                   </li>
                </ul>
              </div>



              <div className="form-group login__action" style={{ bottom: '20px', textAlign: "center", width: '100%' }}>

                <div className="login__submit">
                  <GoogleLogin
                    clientId="162080957438-clkkbq4q7941m5qqlis576imr86482jl.apps.googleusercontent.com"
                    buttonText="Login with Google"
                    onSuccess={this.responseGoogle}
                    onFailure={this.responseGoogle}
                  />
                </div>
              </div>


            </form>
          </div>
        </div>
      )
    }

  }
}