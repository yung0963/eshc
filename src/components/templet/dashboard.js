import React from 'react';
import Video from '../page/video'

const outerDashboard = () => (
  <div>Hello, World!</div>
);

export default class dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
    // this.responseGoogle=this.responseGoogle.bind(this);
  }
  render() {
    const { user_data, user_base } = this.props

    let img_url = user_data.imageUrl
      ? user_data.imageUrl
      : ""

    let name,
      area,
      college,
      dep,
      grade,
      gradeSpan,
      userId
    if (user_base) {

      name = user_base.name
      area = user_base.area
      college = user_base.college
      dep = user_base.dep
      grade = user_base.grade
      userId = user_base.user_id
      if (grade >= 100)
        gradeSpan = <span className="label label-success">已通過</span>
      else {

        gradeSpan = <span className="label label-danger">未通過</span>
      }


    }
    return (

      <div className="dashboard">
        <div className="sidebar">

          <div className="scrollable scrollbar-macosx">
            <div className="sidebar__cont">

              <div className="sidebar__menu">
                <div className="sidebar__title" style={{ fontWeight: "700" }}><i className="fa fa-tags"></i>  使用者資訊</div>
                <div className="ss-widget">
                  <div className="ss-widget__cont" style={{ fontWeight: "700" }}>
                    <br />
                    <div className="ss-widget__row">
                      <img src={img_url} />
                    </div>
                    <br /><br />
                    <div
                      className="ss-widget__row"
                      style={{
                        color: "white"
                      }}>
                      學號：{userId}<br /><br />
                      <span hidden={(name) ? false : true}>
                        姓名：{name}<br /><br />
                        校區：{area}<br /><br />
                        學院：{college}<br /><br />
                        系所：{dep}<br />
                      </span>
                      <br /><br />
                      通過狀態：{gradeSpan}

                    </div>

                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
        <div className="main">
          <div className="main__scroll scrollbar-macosx">
            <div className="main__cont">
              <div className="container-fluid half-padding">
                <div className="template template__blank">
                  <div className="row">
                    <div className="col-md-12">
                      <div className="panel panel-danger">
                        <div className="panel-heading">
                          <h3 className="panel-title">教育訓練系統</h3>
                        </div>
                        <div className="panel-body">

                          <Video user_data={user_data} user_base={user_base} url={this.props.url} changuserInfo={this.props.changuserInfo} />


                        </div>
                      </div>
                    </div>
                  </div>


                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    )

  }
}