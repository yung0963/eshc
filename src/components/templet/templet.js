import React from 'react';
import { GoogleLogout } from 'react-google-login';
import logo from '../../../img/rightlogo.png'
export default class templet extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
        this.logout_btn = this
            .logout_btn
            .bind(this);
    }

    logout_btn() {


        if (confirm("是否登出?"))
            this.props.action()


    }
    render() {
        return (

            <nav className="navbar navbar-static-top header-navbar">
                <div className="header-navbar-mobile">
                    <div className="header-navbar-mobile__menu">
                        <button className="btn" type="button"><i className="fa fa-bars" /></button>
                    </div>
                    <div className="header-navbar-mobile__title">
                        <span>Dashboard</span>
                    </div>
                    <div className="header-navbar-mobile__settings dropdown">
                        <a
                            className="btn dropdown-toggle"

                            data-toggle="dropdown"
                            role="button"
                            aria-haspopup="true"
                            aria-expanded="false"><i className="fa fa-power-off" style={{ color: "white" }} /></a>
                        <ul className="dropdown-menu dropdown-menu-right">
                            <li>
                                <a >Log Out</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="navbar-header">
                    <a className="navbar-brand">
                        <div className="logo text-nowrap">
                            <div className="logo__img">
                                <img src={logo} width="180px" />

                            </div>
                        </div>
                    </a>
                </div>
                <div className="topnavbar">
                    <ul className="nav navbar-nav navbar-left">
                        <li className="active">
                            <a href="#">
                                <span>影片測驗</span>
                            </a>
                        </li>

                    </ul>
                    <ul className="userbar nav navbar-nav">
                        <li className="dropdown"><a className="userbar__settings dropdown-toggle" data-toggle="dropdown" onClick={this.logout_btn} role="button" aria-haspopup="true" aria-expanded="false">
                            <i style={{ fontSize: '20px', color: "white" }} className="fa fa-power-off" ></i></a>

                        </li>
                    </ul>
                </div>
            </nav>

        )
    }
}