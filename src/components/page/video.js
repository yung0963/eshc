
import React from 'react';

import YouTube from 'react-youtube';
import { NotificationManager } from 'react-notifications';

export default class video extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            video_url: null,
            quest: [],
            select: [],
            radioselect: [],
            second: 1,
            mousesecond: 1,
            mousealert: 0,
            play: 1,
            isok: 0
        }
        this.AutoUpdate;
        this.reflashtime = this.reflashtime.bind(this);
        this.setStateAll = this.setStateAll.bind(this);
        this.onChangeRadio = this.onChangeRadio.bind(this)
        this.sendGrade = this.sendGrade.bind(this)
        this.changemousesecond = this.changemousesecond.bind(this)

    }


    stop_time() {
        clearInterval(this.AutoUpdate)
    }
    reflashtime() {

        const knum = this.state.second + 1
        const knum2 = this.state.mousesecond + 1
        this.setState({ second: knum, mousesecond: knum2 });

        if (knum2 > 1800) {
            if (this.state.mousealert == 0) {
                NotificationManager.warning("請於影片以外區域觸碰螢幕，讓系統確認您仍在進行觀看中!", 'warning', 3000);
                this.setState({ mousealert: 1 });
            }
        }

    }



    setStateAll($p1, $p2, $p3) {
        let radio = []
        for (let i = 0; i < $p2.length; i++) {
            radio.push(1)
        }
        this.setState({ video_url: $p1, quest: $p2, select: $p3, radioselect: radio });

    }
    componentDidMount() {
        clearInterval(this.AutoUpdate)
        if (this.state.play == 1)
            this.AutoUpdate = setInterval(() => this.reflashtime(), 1000)
        if (this.props.user_base.mybase)
            this.getVideo_info(this.props)

        $(document).on('mousemove', 'html', () => {
            this.changemousesecond()

        });

    }
    changemousesecond() {

        this.setState({ mousesecond: 0, mousealert: 0 });
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.user_base.mybase != this.props.user_base.mybase) {
            this.getVideo_info(nextProps)
        }



    }

    getVideo_info(Props) {
        const { user_base } = Props

        const rej = {
            "user_base": user_base.mybase

        }

        fetch(this.props.url + "get_video_info",
            {
                method: 'post',
                body: JSON.stringify(rej)
            }).then(response => Promise.all([response, response.json()])).then(([response, json]) => {
                if (response.status === 200) {

                    if (json.status == "success") {
                        console.log("success", json)

                        this.setStateAll(json.video_url, json.quest, json.select)

                    } else {
                        NotificationManager.warning(json.msg, 'warning', 3000);

                    }
                }
                else {

                }
            }).catch((error) => {
                console.log(error)
            });
    }


    componentWillUnmount() {
        clearInterval(this.AutoUpdate)


    }
    onChangeRadio(e) {
        let ename = e.name.replace("q", "")

        let rad = this.state.radioselect
        rad[ename] = parseInt(e.value)

        this.setState({ radioselect: rad });

    }

    sendGrade() {
        console.log("send")
        const rej = {
            "user_base": this.props.user_base.mybase,
            "ans": this.state.radioselect
        }

        fetch(this.props.url + "get_video_Grade",
            {
                method: 'post',
                body: JSON.stringify(rej)
            }).then(response => Promise.all([response, response.json()])).then(([response, json]) => {
                if (response.status === 200) {

                    if (json.status == "success") {
                        NotificationManager.info(json.msg);

                        this.props.changuserInfo()


                    } else {
                        NotificationManager.error(json.msg, '請檢查');

                    }
                }
                else {

                }
            }).catch((error) => {
                console.log(error)
            });
    }


    render() {
        let videomustwatch = 8000
        let videomustwatchleave = 9300
        let youtube = null
        const opts = {
            height: '550',
            width: '900',
            playerVars: { // https://developers.google.com/youtube/player_parameters
                autoplay: 0,
                showinfo: 0,
                rel: 0,
                controls: 0,
                fs: 0,
                modestbranding: 0
            },
            // controls: 0
        }
        if (this.state.video_url != null) {
            let video = this.state.video_url.split("v=")[1]

            youtube = <YouTube
                videoId={video}
                opts={opts}
                onReady={this._onReady}
                onPlay={this._onPlay}

            />
        }
        const { select, quest } = this.state

        const user_base = this.props.user_base


        let PrintBtn = < div className="alert alert-danger alert-dismissible" role="alert" > <i className="alert-ico fa fa-fw fa-ban"></i> <strong>提醒您～<br /><br />
            -影片播放大約2.5小時，過程中若有暫停、快轉或跳頁，都不計入時數。<br /><br />
            -以電腦觀看影片，請於每３０分鐘於影片以外區域移動滑鼠；若使用手機觀看，則於每３０分鐘請於影片以外區域觸碰螢幕，讓系統確認您仍在進行觀看中！(如以全螢幕觀看，則需取消全螢幕來進行上述動作)<br /><br />
            -請於影片結束後２０分鐘內進行測驗！若超過２０分鐘未動作，則需重新觀看影片！ </strong></div >

        if (this.state.second > videomustwatch)
            PrintBtn = < div className="alert alert-success alert-dismissible" role="alert" > <i className="alert-ico fa fa-fw fa-check"></i> <strong>請開始測驗! </strong></div >


        const q1 = (quest.length > 0) ? quest : []
        const q2 = (select.length > 0) ? select : []


        let checkbrtn = null
        if (this.state.second > videomustwatch)
            checkbrtn = <button className="btn btn-info btn-lg" onClick={this.sendGrade} type="button" >送出測驗</button>
        if (this.state.second > videomustwatchleave)
            checkbrtn = <button className="btn btn-danger btn-lg" type="button" >閒置過久..請重新觀看影片</button>

        if (user_base)
            if (user_base.grade >= 100 || this.state.isok == 1)
                checkbrtn = <button className="btn btn-info btn-lg" type="button" disabled>已做完測驗</button>


        return (
            <div>
                <div style={{ textAlign: "center" }}>
                    {youtube}
                </div>
                <br /><br /><br />


                {PrintBtn}
                {
                    q1.map((d, i) => {

                        if (this.state.second > videomustwatch)
                            return (
                                <div key={"q1" + i}>
                                    <h1 > {(i + 1) + "." + d}</h1>
                                    {
                                        q2[i].map((dd, j) => {

                                            return (

                                                <div style={{ marginLeft: '35px' }} key={"h4" + i + j} className="radio radio-primary">
                                                    <input className="radio" type="radio" key={"input" + i + "r" + j} name={"q" + i} value={j + 1} checked={((j + 1) == this.state.radioselect[i]) ? "true" : ""} onChange={(e) => this.onChangeRadio(e.target)} />
                                                    <label >{dd}</label>
                                                </div>
                                            )
                                        })
                                    }


                                </div>

                            )

                    })


                }
                {checkbrtn}
            </div>



        )



    }

    _onReady(event) {
        // access to player in all event handlers via event.target
        // console.log(event.target.getDuration)

    }
    _onPlay(event) {
        // access to player in all event handlers via event.target
        console.log("play")

    }
}