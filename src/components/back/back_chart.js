import React, { Component } from 'react';
import Highcharts from 'react-highcharts'

const theme = {
    colors: ['#7cb5ec', '#f7a35c', '#90ee7e', '#7798BF', '#aaeeee', '#ff0066',
        '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
    chart: {
        backgroundColor: null,
        style: {
            fontFamily: 'Dosis, sans-serif'
        }
    },
    title: {
        style: {
            fontSize: '16px',
            fontWeight: 'bold',
            textTransform: 'uppercase'
        }
    },
    tooltip: {
        borderWidth: 0,
        backgroundColor: 'rgba(219,219,216,0.8)',
        shadow: false
    },
    legend: {
        itemStyle: {
            fontWeight: 'bold',
            fontSize: '13px'
        }
    },
    xAxis: {
        gridLineWidth: 1,
        labels: {
            style: {
                fontSize: '12px'
            }
        }
    },
    yAxis: {
        minorTickInterval: 'auto',
        title: {
            style: {
                textTransform: 'uppercase'
            }
        },
        labels: {
            style: {
                fontSize: '12px'
            }
        }
    },
    plotOptions: {
        candlestick: {
            lineColor: '#404048'
        }
    },


    // General
    background2: '#F0F0EA', xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    },
    series: [{
        data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 295.6, 454.4]
    }]

};



export default class back_chart extends Component {
    constructor(props) {
        super(props)

        this.state = {
            mybase: this.props.user_base.mybase,
            jsondata: {
                col1: null,
                col2: null,
                col3: null,
                data1: null,
                data2: null,
                data3: null,
            }

        };
        this.getChart = this.getChart.bind(this)
    };
    getChart() {
        const rej = {
            "user_base": this.state.mybase,

        }
        fetch(this.props.url + "b_get_chart",
            {
                method: 'post',
                body: JSON.stringify(rej)
            }).then(response => Promise.all([response, response.json()])).then(([response, json]) => {
                if (response.status === 200) {

                    if (json.status == "success") {


                        this.setState({ jsondata: json })
                    }
                }
                else {

                }
            }).catch((error) => {
                console.log(error)
            });
    }

    componentDidMount() {

        this.getChart()


    }
    render() {
        var config = {
            chart: {
                type: 'column'
            },
            title: {
                text: '第一校區 各系所新生出席率'
            },
            xAxis: {
                title: {
                    text: ''
                },
                categories: this.state.jsondata.col1 || []

            },
            yAxis: {
                title: {
                    text: ''
                },

                labels: {
                    formatter: function () {
                        return this.value + ' %';
                    }
                },
                crosshair: true
            },
            plotOptions: {
                column: {
                    dataLabels: {
                        enabled: true,
                        formatter: function () {
                            return this.y + ' %';
                        }
                    }
                }
            },
            tooltip: {
                backgroundColor: '#FCFFC5',
                borderColor: 'black',
                borderRadius: 10,
                borderWidth: 3
            },
            series: [{
                name: '107',
                data: this.state.jsondata.data1 || []
            },
            ]
        };


        var config2 = {
            chart: {
                type: 'column'
            },
            title: {
                text: '第一校區 大學部新生出席率 '
            },
            xAxis: {
                title: {
                    text: ''
                },
                categories: this.state.jsondata.col2 || []

            },
            yAxis: {
                title: {
                    text: ''
                },

                labels: {
                    formatter: function () {
                        return this.value + ' %';
                    }
                },
                crosshair: true
            },
            plotOptions: {
                column: {
                    dataLabels: {
                        enabled: true,
                        formatter: function () {
                            return this.y + ' %';
                        }
                    }
                }
            },
            tooltip: {
                backgroundColor: '#FCFFC5',
                borderColor: 'black',
                borderRadius: 10,
                borderWidth: 3
            },
            series: [{
                name: '107',
                data: this.state.jsondata.data2 || []
            },
            ]
        };


        var config3 = {
            chart: {
                type: 'column'
            },
            title: {
                text: '第一校區 碩士班出席率 '
            },
            xAxis: {
                title: {
                    text: ''
                },
                categories: this.state.jsondata.col3 || []

            },
            yAxis: {
                title: {
                    text: ''
                },

                labels: {
                    formatter: function () {
                        return this.value + ' %';
                    }
                },
                crosshair: true
            },
            plotOptions: {
                column: {
                    dataLabels: {
                        enabled: true,
                        formatter: function () {
                            return this.y + ' %';
                        }
                    }
                }
            },
            tooltip: {
                backgroundColor: '#FCFFC5',
                borderColor: 'black',
                borderRadius: 10,
                borderWidth: 3
            },
            series: [{
                name: '107',
                data: this.state.jsondata.data3 || []
            },
            ]
        };
        var config4 = {
            chart: {
                type: 'column'
            },
            title: {
                text: '建功/燕巢校區 各系所新生出席率 '
            },
            xAxis: {
                title: {
                    text: ''
                },
                categories: this.state.jsondata.col4 || []

            },
            yAxis: {
                title: {
                    text: ''
                },

                labels: {
                    formatter: function () {
                        return this.value + ' %';
                    }
                },
                crosshair: true
            },
            plotOptions: {
                column: {
                    dataLabels: {
                        enabled: true,
                        formatter: function () {
                            return this.y + ' %';
                        }
                    }
                }
            },
            tooltip: {
                backgroundColor: '#FCFFC5',
                borderColor: 'black',
                borderRadius: 10,
                borderWidth: 3
            },
            series: [{
                name: '107',
                data: this.state.jsondata.data4 || []
            },
            ]
        };
        var config5 = {
            chart: {
                type: 'column'
            },
            title: {
                text: '建功/燕巢校區 大學部新生出席率 '
            },
            xAxis: {
                title: {
                    text: ''
                },
                categories: this.state.jsondata.col5 || []

            },
            yAxis: {
                title: {
                    text: ''
                },

                labels: {
                    formatter: function () {
                        return this.value + ' %';
                    }
                },
                crosshair: true
            },
            plotOptions: {
                column: {
                    dataLabels: {
                        enabled: true,
                        formatter: function () {
                            return this.y + ' %';
                        }
                    }
                }
            },
            tooltip: {
                backgroundColor: '#FCFFC5',
                borderColor: 'black',
                borderRadius: 10,
                borderWidth: 3
            },
            series: [{
                name: '107',
                data: this.state.jsondata.data5 || []
            },
            ]
        };
        var config6 = {
            chart: {
                type: 'column'
            },
            title: {
                text: '建功/燕巢校區 碩士班出席率 '
            },
            xAxis: {
                title: {
                    text: ''
                },
                categories: this.state.jsondata.col6 || []

            },
            yAxis: {
                title: {
                    text: ''
                },

                labels: {
                    formatter: function () {
                        return this.value + ' %';
                    }
                },
                crosshair: true
            },
            plotOptions: {
                column: {
                    dataLabels: {
                        enabled: true,
                        formatter: function () {
                            return this.y + ' %';
                        }
                    }
                }
            },
            tooltip: {
                backgroundColor: '#FCFFC5',
                borderColor: 'black',
                borderRadius: 10,
                borderWidth: 3
            },
            series: [{
                name: '107',
                data: this.state.jsondata.data6 || []
            },
            ]
        };
        var config7 = {
            chart: {
                type: 'column'
            },
            title: {
                text: '楠梓/旗津 各系所新生出席率 '
            },
            xAxis: {
                title: {
                    text: ''
                },
                categories: this.state.jsondata.col7 || []

            },
            yAxis: {
                title: {
                    text: ''
                },

                labels: {
                    formatter: function () {
                        return this.value + ' %';
                    }
                },
                crosshair: true
            },
            plotOptions: {
                column: {
                    dataLabels: {
                        enabled: true,
                        formatter: function () {
                            return this.y + ' %';
                        }
                    }
                }
            },
            tooltip: {
                backgroundColor: '#FCFFC5',
                borderColor: 'black',
                borderRadius: 10,
                borderWidth: 3
            },
            series: [{
                name: '107',
                data: this.state.jsondata.data7 || []
            },
            ]
        };
        var config8 = {
            chart: {
                type: 'column'
            },
            title: {
                text: '楠梓/旗津 大學部新生出席率 '
            },
            xAxis: {
                title: {
                    text: ''
                },
                categories: this.state.jsondata.col8 || []

            },
            yAxis: {
                title: {
                    text: ''
                },

                labels: {
                    formatter: function () {
                        return this.value + ' %';
                    }
                },
                crosshair: true
            },
            plotOptions: {
                column: {
                    dataLabels: {
                        enabled: true,
                        formatter: function () {
                            return this.y + ' %';
                        }
                    }
                }
            },
            tooltip: {
                backgroundColor: '#FCFFC5',
                borderColor: 'black',
                borderRadius: 10,
                borderWidth: 3
            },
            series: [{
                name: '107',
                data: this.state.jsondata.data8 || []
            },
            ]
        };
        var config9 = {
            chart: {
                type: 'column'
            },
            title: {
                text: '楠梓/旗津 碩士班出席率 '
            },
            xAxis: {
                title: {
                    text: ''
                },
                categories: this.state.jsondata.col9 || []

            },
            yAxis: {
                title: {
                    text: ''
                },

                labels: {
                    formatter: function () {
                        return this.value + ' %';
                    }
                },
                crosshair: true
            },
            plotOptions: {
                column: {
                    dataLabels: {
                        enabled: true,
                        formatter: function () {
                            return this.y + ' %';
                        }
                    }
                }
            },
            tooltip: {
                backgroundColor: '#FCFFC5',
                borderColor: 'black',
                borderRadius: 10,
                borderWidth: 3
            },
            series: [{
                name: '107',
                data: this.state.jsondata.data9 || []
            },
            ]
        };
        var config10 = {
            chart: {
                type: 'column'
            },
            title: {
                text: '楠梓/旗津 碩專班出席率 '
            },
            xAxis: {
                title: {
                    text: ''
                },
                categories: this.state.jsondata.col10 || []

            },
            yAxis: {
                title: {
                    text: ''
                },

                labels: {
                    formatter: function () {
                        return this.value + ' %';
                    }
                },
                crosshair: true
            },
            plotOptions: {
                column: {
                    dataLabels: {
                        enabled: true,
                        formatter: function () {
                            return this.y + ' %';
                        }
                    }
                }
            },
            tooltip: {
                backgroundColor: '#FCFFC5',
                borderColor: 'black',
                borderRadius: 10,
                borderWidth: 3
            },
            series: [{
                name: '107',
                data: this.state.jsondata.data10 || []
            },
            ]
        };
        Highcharts.Highcharts.setOptions(theme)
        return (
            <div style={{ backgroundColor: "white" }}>
                <h1>第一校區</h1>
                <Highcharts config={config}></Highcharts >
                <Highcharts config={config2}></Highcharts >
                <Highcharts config={config3}></Highcharts >

                <h1>建工/燕巢校區</h1>
                <Highcharts config={config4}></Highcharts >
                <Highcharts config={config5}></Highcharts >
                <Highcharts config={config6}></Highcharts >
                <h1>楠梓/旗津校區</h1>
                <Highcharts config={config7}></Highcharts >
                <Highcharts config={config8}></Highcharts >
                <Highcharts config={config9}></Highcharts >
                <Highcharts config={config10}></Highcharts >
            </div>
        )
    }

};


