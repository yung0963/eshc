import React from 'react';

import logo from '../../../img/rightlogo.png'
export default class back_templet extends React.Component {
  constructor(props) {
    super(props);
    this.state = {


    }
    this.logout_btn = this.logout_btn.bind(this);
  }

  logout_btn() {

    if (confirm("是否登出?"))
      this.props.action()


  }

  ch_page(page) {
    this.props.changeMyPage(page)

  }
  render() {
    return (

      <nav className="navbar navbar-static-top header-navbar">
        <div className="header-navbar-mobile">
          <div className="header-navbar-mobile__menu">
            <button className="btn" type="button"><i className="fa fa-bars" /></button>
          </div>
          <div className="header-navbar-mobile__title"><span>Dashboard</span></div>
          <div className="header-navbar-mobile__settings dropdown"><a className="btn dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i className="fa fa-power-off" style={{ color: "white" }} /></a>
            <ul className="dropdown-menu dropdown-menu-right">
              <li><a href="#">Log Out</a></li>
            </ul>
          </div>
        </div>
        <div className="navbar-header"><a className="navbar-brand">
          <div className="logo text-nowrap">
            <div className="logo__img">
              <img src={logo} width="180px" />

            </div>
          </div></a></div>
        <div className="topnavbar" >
          <ul className="nav navbar-nav navbar-left">
            <li className={(this.props.page == "student") ? "active" : ""} onClick={(e) => this.ch_page('student')}> <a><span>學生管理</span></a></li>
            <li className={(this.props.page == "table") ? "active" : ""} onClick={(e) => this.ch_page('table')} className={(this.props.user_base.status == "success") ? '' : "hiddenelem"}><a><span>統計表</span></a></li>
            <li className={(this.props.page == "chart") ? "active" : ""} onClick={(e) => this.ch_page('chart')} className={(this.props.user_base.status == "success") ? '' : "hiddenelem"}><a><span>歷年數據統計圖</span></a></li>
            <li className={(this.props.page == "auth") ? "active" : ""} onClick={(e) => this.ch_page('auth')} className={(this.props.user_base.status == "success") ? '' : "hiddenelem"}><a><span>權限統計</span></a></li>
            <li className={(this.props.page == "video") ? "active" : ""} onClick={(e) => this.ch_page('video')} className={(this.props.user_base.status == "success") ? '' : "hiddenelem"}><a><span>影片/測驗管理</span></a></li>



          </ul>
          <ul className="userbar nav navbar-nav">
            <li className="dropdown"><a className="userbar__settings dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <i style={{ fontSize: '20px', color: "white" }} onClick={this.logout_btn} className="fa fa-power-off" ></i></a>

            </li>
          </ul>
        </div>
      </nav>



    )
  }
}