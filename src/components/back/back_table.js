import React, { Component } from 'react';
import Highcharts from 'react-highcharts'




export default class back_table extends Component {
    constructor(props) {
        super(props)

        this.state = {
            mybase: this.props.user_base.mybase,
            jsondata: "請稍後...",
            jsondata2: "請稍後...",
            jsondata3: "請稍後...",

        };
        this.getChart = this.getChart.bind(this)
        this.getChart2 = this.getChart2.bind(this)
        this.getChart3 = this.getChart3.bind(this)
    };
    getChart() {
        const rej = {
            "user_base": this.state.mybase,

        }
        fetch(this.props.url + "b_get_sum",
            {
                method: 'post',
                body: JSON.stringify(rej)
            }).then(response => Promise.all([response, response.json()])).then(([response, json]) => {
                if (response.status === 200) {




                    this.setState({ jsondata: json.strHtml })


                }
                else {

                }
            }).catch((error) => {
                console.log(error)
            });

    }
    getChart2() {
        const rej = {
            "user_base": this.state.mybase,

        }
        fetch(this.props.url + "b_get_sum2",
            {
                method: 'post',
                body: JSON.stringify(rej)
            }).then(response => Promise.all([response, response.json()])).then(([response, json]) => {
                if (response.status === 200) {




                    this.setState({ jsondata2: json.strHtml })


                }
                else {

                }
            }).catch((error) => {
                console.log(error)
            });

    }
    getChart3() {
        const rej = {
            "user_base": this.state.mybase,

        }
        fetch(this.props.url + "b_get_sum3",
            {
                method: 'post',
                body: JSON.stringify(rej)
            }).then(response => Promise.all([response, response.json()])).then(([response, json]) => {
                if (response.status === 200) {




                    this.setState({ jsondata3: json.strHtml })


                }
                else {

                }
            }).catch((error) => {
                console.log(error)
            });
    }
    componentDidMount() {

        this.getChart()
        this.getChart2()
        this.getChart3()


    }
    render() {
        let jsonda = this.state.jsondata || "";
        let jsonda2 = this.state.jsondata2 || "";
        let jsonda3 = this.state.jsondata3 || "";

        return (
            <div style={{ backgroundColor: "white" }}>
                <h1>第一校區</h1>
                <div dangerouslySetInnerHTML={{ __html: jsonda }} />
                <h1>建工/燕巢校區</h1>
                <div dangerouslySetInnerHTML={{ __html: jsonda2 }} />
                <h1>楠梓/旗津校區</h1>
                <div dangerouslySetInnerHTML={{ __html: jsonda3 }} />
            </div>
        )
    }

};


