import React from 'react';
import * as XLSX from 'xlsx';
import Dropzone from 'react-dropzone'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { NotificationManager } from 'react-notifications';

export default class back_student extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

            file_type: ".csv,.xls,.xlsx",
            mybase: this.props.user_base.mybase,
            student_data: null,

        }

        this.onAddRow = this.onAddRow.bind(this)
        this.onDrop = this.onDrop.bind(this)
        this.getUserinfo = this.getUserinfo.bind(this)
        this.onDeleteRow = this.onDeleteRow.bind(this)
        this.afterSaveCell = this.afterSaveCell.bind(this)

    }
    onDrop(acceptedFiles) {

        let rABS = false;// true: readAsBinaryString ; false: readAsArrayBuffer


        acceptedFiles.forEach(file => {

            const reader = new FileReader();
            reader.onload = () => {

                let jsondata

                let data = reader.result;
                // do whatever you want with the file content
                if (!rABS) data = new Uint8Array(data);
                const workbook = XLSX.read(data, { type: rABS ? 'binary' : 'array' });
                const first_worksheet = workbook.Sheets[workbook.SheetNames[0]];
                jsondata = XLSX.utils.sheet_to_json(first_worksheet, { header: 1, defval: null });
                const rej = {
                    "user_base": this.state.mybase,
                    "jsondata": jsondata,

                }
                NotificationManager.warning("上傳中請稍後.....", "提醒", 5000);
                fetch(this.props.url + "b_InertUserInfo_JSON",
                    {
                        method: 'post',
                        body: JSON.stringify(rej)
                    }).then(response => Promise.all([response, response.json()])).then(([response, json]) => {
                        if (response.status === 200) {

                            if (json.status == "success") {


                                this.setState({ student_data: json.data })
                                this.getUserinfo()
                            }
                            NotificationManager.info(json.msg);
                        }
                        else {

                        }
                    }).catch((error) => {
                        console.log(error)
                    });




                this.props.actions.CRUD_schedule(['showdata'], 'MODIFY_SCHEDULE', jsondata)
                this.props.actions.CRUD_schedule(['isupload'], 'MODIFY_SCHEDULE', false)


            };
            reader.onabort = () => console.log('file reading was aborted');
            reader.onerror = () => console.log('file reading has failed');


            if (rABS) reader.readAsBinaryString(file); else reader.readAsArrayBuffer(file);


        });

    }
    componentDidMount() {
        this.getUserinfo()
    }
    getUserinfo() {
        const rej = {
            "user_base": this.state.mybase,

        }
        fetch(this.props.url + "b_getUserInfo",
            {
                method: 'post',
                body: JSON.stringify(rej)
            }).then(response => Promise.all([response, response.json()])).then(([response, json]) => {
                if (response.status === 200) {

                    if (json.status == "success") {


                        this.setState({ student_data: json.data })

                    }
                }
                else {

                }
            }).catch((error) => {
                console.log(error)
            });
    }

    render() {
        const note2 = {
            '休學': '休學',
            '退學': '退學',
            '放棄': '放棄',
            '取消入學資格': '取消入學資格',
            '外籍生': '外籍生',
            '': ''
        };
        const note = [
            '休學',
            '退學',
            '放棄',
            '取消入學資格',
            '外籍生',
            ''

        ];

        const area2 = [
            '第一校區',
            '楠梓校區',
            '旗津校區',
            '建工校區',
            '燕巢校區'

        ];
        const area = {
            '第一校區': '第一校區',
            '楠梓校區': '楠梓校區',
            '旗津校區': '旗津校區',
            '建工校區': '建工校區',
            '燕巢校區': '燕巢校區'
        };
        const college = {
            '工學院': '工學院',
            '電資學院': '電資學院',
            '管理學院': '管理學院',
            '海工學院': '海工學院',
            '水圈學院': '水圈學院',
            '海事學院': '海事學院',
            '人文社會學院': '人文社會學院',
            '跨校生': '跨校生'
        };
        const college2 = [
            '工學院',
            '電資學院',
            '管理學院',
            '海工學院',
            '水圈學院',
            '海事學院',
            '人文社會學院',
            '跨校生'

        ];
        // const school_sys = {
        //     '四技': '四技',
        //     '進四技': '進四技',
        //     '二技': '二技',
        //     '研究所': '研究所',
        //     '進修部四技':'進修部四技',
        //     '博士班': '博士班',
        //     '進修部碩士':'進修部碩士',
        //     '進修部碩士春季班':'進修部碩士春季班',
        //     '日間部五專':'日間部五專'
        // }

        // const school_sys2 = [
        //     '四技',
        //     '進四技',
        //     '二技',
        //     '研究所',
        //     '博士班',
        //     '進修部四技',
        //     '進修部碩士',
        //     '進修部碩士春季班',
        //     '日間部五專'

        // ]

        const options = {
            btnGroup: this.createCustomButtonGroup

        };
        const cellEdit = {
            mode: 'click', // click cell to edit
            afterSaveCell: this.afterSaveCell,
            beforeSaveCell: this.beforeSaveCell,
        };
        const selectRow = {
            mode: 'radio'
        };
        let dropzonec = true
        if (this.props.user_base.status == "assis") {
            dropzonec = false

        }

        return (

            <div>

                {(dropzonec) ? <Dropzone
                    accept={this.state.file_type}
                    onDrop={this.onDrop.bind(this)} className="dropzone dz-clickable">
                    <div className="drop_zone">
                        <span hidden={(this.state.isupload) ? true : ''}>
                            新增學生資料
                                    </span>
                        <span hidden={(this.state.isupload) ? '' : true} >
                            <i className="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                            <span className="sr-only">Loading...</span>

                        </span>
                    </div>
                </Dropzone>
                    :
                    ""}
                <br /><br />

                <BootstrapTable selectRow={selectRow} data={this.state.student_data || []} options={options} exportCSV cellEdit={cellEdit} pagination >


                    <TableHeaderColumn width="120px" editable={false} dataField='area' filterFormatted dataFormat={this.enumFormatter} formatExtraData={area}
                        filter={{ type: 'SelectFilter', options: area }} dataSort={true}>校區</TableHeaderColumn>


                    <TableHeaderColumn width="120px" editable={false} dataField='college' filterFormatted dataFormat={this.enumFormatter} formatExtraData={college}
                        filter={{ type: 'SelectFilter', options: college }} dataSort={true}>學院</TableHeaderColumn>
                    <TableHeaderColumn editable={false} filter={{ type: 'TextFilter', delay: 500 }} dataField='dep' dataSort={true}>科系</TableHeaderColumn>
                    <TableHeaderColumn width="120px" editable={false} dataField='school_sys'
                        filter={{ type: 'TextFilter', delay: 500 }} dataSort={true}>學制</TableHeaderColumn>
                    <TableHeaderColumn width="100px" editable={false} dataField='name' filter={{ type: 'TextFilter', delay: 500 }}>姓名</TableHeaderColumn>
                    <TableHeaderColumn width="150px" editable={false} dataField='user_id' filter={{ type: 'TextFilter', delay: 500 }} dataSort={true} isKey>帳號</TableHeaderColumn>
                    <TableHeaderColumn editable={false} dataField='get_date' dataSort={true}>取得成績日期</TableHeaderColumn>
                    <TableHeaderColumn width="80px" editable={false} dataField='grade' dataSort={true}>分數</TableHeaderColumn>
                    <TableHeaderColumn dataField='note' editable={{ type: 'select', options: { values: note } }}>備註(下拉選單選擇後，按Enter儲存)</TableHeaderColumn>

                </BootstrapTable>
            </div>


        )
    }
    createCustomButtonGroup = props => {
        return (
            <ButtonGroup className='my-custom-class' sizeClass='btn-group-md'>
                {props.exportCSVBtn}
                <a style={{ marginLeft: "10px" }} href="http://eshc.osha.web.nkust.edu.tw/page/file/test.xlsx"><button type='button'
                    className={`btn btn-info`}>
                    新增學生資料表單
        </button></a>
            </ButtonGroup>
        );
    }

    enumFormatter(cell, row, enumObject) {
        return enumObject[cell];
    }

    createCustomInsertButton = (onClick) => {
        return (
            <InsertButton
                btnText='新增資料'
                btnContextual='btn-warning'
                className='my-custom-class'
                btnGlyphicon='glyphicon-edit'
                onClick={() => this.handleInsertButtonClick(onClick)} />
        );
    }

    handleInsertButtonClick = (onClick) => {
        // Custom your onClick event here,
        // it's not necessary to implement this function if you have no any process before onClick
        console.log('This is my custom function for InserButton click event');
        onClick();
    }

    onAddRow(row) {
        console.log(row)
        const rej = {
            "user_base": this.state.mybase,
            "area": row.area,
            "college": row.college,
            "dep": row.dep,
            "school_sys": row.school_sys,
            "name": row.name,
            "user_id": row.user_id

        }
        fetch(this.props.url + "b_InertUserInfo",
            {
                method: 'post',
                body: JSON.stringify(rej)
            }).then(response => Promise.all([response, response.json()])).then(([response, json]) => {
                if (response.status === 200) {

                    if (json.status == "success") {


                        this.setState({ student_data: json.data })
                        this.getUserinfo()
                    }
                    NotificationManager.info(json.msg);
                }
                else {
                    NotificationManager.warning("錯誤");
                }
            }).catch((error) => {
                NotificationManager.error("錯誤");
            });
    }

    afterSaveCell(row, cellName, cellValue) {
        console.log(row)
        const rej = {
            "user_base": this.state.mybase,
            "s_id": row.s_id,
            "area": row.area,
            "college": row.college,
            "dep": row.dep,
            "school_sys": row.school_sys,
            "name": row.name,
            "user_id": row.user_id,
            "get_date": row.get_date,
            "grade": row.grade,
            "note": row.note

        }
        fetch(this.props.url + "b_update_user",
            {
                method: 'post',
                body: JSON.stringify(rej)
            }).then(response => Promise.all([response, response.json()])).then(([response, json]) => {
                if (response.status === 200) {

                    if (json.status == "success") {

                        NotificationManager.info(json.msg);
                        this.getVideo_info(this.props)


                    }

                }
                else {

                }
            }).catch((error) => {
                console.log(error)
            });
    }
    createCustomDeleteButton = (onClick) => {
        return (
            <InsertButton
                btnText='刪除資料'
                btnContextual='btn-danger'
                className='my-custom-class'
                btnGlyphicon='glyphicon-edit'
                onClick={() => this.handleDeleteButtonClick(onClick)} />
        );
    }
    handleDeleteButtonClick = (onClick) => {

        onClick();
    }
    onDeleteRow(row) {


        const rej = {
            "user_base": this.state.mybase,
            "id": row,


        }
        fetch(this.props.url + "b_delete_user",
            {
                method: 'post',
                body: JSON.stringify(rej)
            }).then(response => Promise.all([response, response.json()])).then(([response, json]) => {
                if (response.status === 200) {

                    if (json.status == "success") {

                        this.getUserinfo()


                    }
                    NotificationManager.info(json.msg);
                }
                else {

                }
            }).catch((error) => {
                console.log(error)
            });
    }

}