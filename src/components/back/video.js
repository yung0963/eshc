import React from 'react';
import Select from 'react-select';
import { NotificationManager } from 'react-notifications';
export default class video extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            stateTime: new Date(),
            video_url: "https://www.youtube.com/watch?v=yk2CUjbyyQY",
            quest_json: [],
            quest_ans: [],
            quest_selectitem: [],
            select_json: 0
        }

    }

    onChangeQuest($par) {


        this.setState({ select_json: ($par - 1) })
        console.log($par)


    }

    onChangeQuest_s($par) {

        let tempjs = this.state.quest_selectitem


        switch ($par.id) {
            case "se1":
                tempjs[this.state.select_json][0] = $par.value
                this.setState({ quest_selectitem: tempjs })
                break;
            case "se2":
                tempjs[this.state.select_json][1] = $par.value
                this.setState({ quest_selectitem: tempjs })
                break;
            case "se3":
                tempjs[this.state.select_json][2] = $par.value
                this.setState({ quest_selectitem: tempjs })
                break;
            case "se4":
                tempjs[this.state.select_json][3] = $par.value
                this.setState({ quest_selectitem: tempjs })
                break;
            default:
                break;

        }

    }
    onChangeQuest_title($par) {
        let tem = this.state.quest_json;
        tem[this.state.select_json] = $par
        this.setState({ quest_json: tem })
    }
    onChangeAns($par) {

        let tem = this.state.quest_ans;
        tem[this.state.select_json] = $par
        this.setState({ quest_ans: tem })
    }
    onClickAdd() {
        let tempjson = this.state.quest_json
        tempjson.push("題目")
        let tempjson2 = this.state.quest_selectitem
        let mm = ["選項", "選項", "選項", "選項"]
        tempjson2.push(mm)
        let tempjson3 = this.state.quest_ans
        tempjson3.push(1)
        this.setState({ quest_json: tempjson })
        this.setState({ quest_selectitem: tempjson2 })
        this.setState({ quest_ans: tempjson3 })
    }

    onClickremove() {

        let tempjson = this.state.quest_json
        if (Object.keys(tempjson).length > 1) {

            tempjson.splice(this.state.select_json, 1)
            let tempjson2 = this.state.quest_selectitem
            tempjson2.splice(this.state.select_json, 1)
            let tempjson3 = this.state.quest_ans
            tempjson3.splice(this.state.quest_ans, 1)

            this.setState({ quest_json: tempjson })
            this.setState({ quest_selectitem: tempjson2 })
            this.setState({ quest_ans: tempjson3 })
            if (this.state.select_json == Object.keys(tempjson).length)
                this.setState({ select_json: Object.keys(tempjson).length - 1 })
        }
        else {

            NotificationManager.warning('至少1題', 'Close after 3000ms', 3000);
        }
    }
    onClickSave() {
        const rej = {
            "user_base": this.props.user_base.mybase,
            "video_url": this.state.video_url,
            "quest_json": this.state.quest_json,
            "quest_ans": this.state.quest_ans,
            "quest_selectitem": this.state.quest_selectitem,

        }
        fetch(this.props.url + "b_save_video",
            {
                method: 'post',
                body: JSON.stringify(rej)
            }).then(response => Promise.all([response, response.json()])).then(([response, json]) => {
                if (response.status === 200) {

                    if (json.status == "success") {
                        setCookie("userInfo", JSON.stringify(params.profileObj), 10)
                        setCookie("userBase", JSON.stringify(json), 10)
                        this.setUserInfo(params.profileObj)
                        this.setUserBase(json)

                        NotificationManager.info(json.msg);
                    } else {

                        NotificationManager.warning(json.msg, '', 3000);


                    }
                }
                else {

                }
            }).catch((error) => {
                console.log(error)
            });
    }
    onChangeVideo($par) {
        console.log($par)
        this.setState({ video_url: $par })
    }
    componentDidMount() {
        const rej = {
            "user_base": this.props.user_base.mybase


        }
        fetch(this.props.url + "b_get_video_info", {
            method: 'post',
            body: JSON.stringify(rej)
        })
            .then(response => Promise.all([
                response, response.json()
            ]))
            .then(([response, json]) => {
                if (response.status === 200) {
                    this.setState({ stateTime: new Date() })
                    if (json.status == "success") {
                        this.setState({ stateTime: new Date(), video_url: json.video_url, quest_json: json.quest, quest_ans: json.ans, quest_selectitem: json.select })

                    } else {
                        // console.log("b_get_video_info",json)
                        this.setState({ stateTime: new Date() })

                    }

                }
            })
            .catch((error) => {
                console.log(error)
            });


    }
    componentWillUpdate(nextProps, nextState) {
        if (nextState.stateTime != this.state.stateTime) {
            if (Object.keys(nextState.quest_json).length == 0) {
                let tempjson = nextState.quest_json

                for (let i = 1; i <= 5; i++) {
                    tempjson.push("第" + i + "題...")
                }
                this.setState({ quest_json: tempjson })

                let tempjson2 = nextState.quest_selectitem


                for (let i = 0; i < 5; i++) {
                    let te = []
                    te.push("第" + (i + 1) + "題 第1選項...")
                    te.push("第" + (i + 1) + "題 第2選項...")
                    te.push("第" + (i + 1) + "題 第3選項...")
                    te.push("第" + (i + 1) + "題 第4選項...")


                    tempjson2.push(te)
                }
                this.setState({ quest_selectitem: tempjson2 })

                let tempjson3 = nextState.quest_ans

                for (let i = 1; i <= 5; i++) {
                    tempjson3.push(1)
                }
                this.setState({ quest_ans: tempjson3 })

            }
        }
    }

    render() {


        if (Object.keys(this.state.quest_selectitem).length > 0) {


            const quest_itme = this.state.quest_selectitem
            const quest_title = this.state.quest_json
            const quest_ans = this.state.quest_ans

            // console.log("quest_ans", quest_ans)
            const qut = quest_itme[this.state.select_json]



            return (
                <div>
                    <div className="col-md-6">
                        <div className="panel panel-danger">

                            <div className="panel-body">
                                <div className="form-horizontal">
                                    <div className="form-group">
                                        <label className="col-sm-2 control-label">Youtube URL</label>
                                        <div className="col-sm-10">
                                            <input
                                                className="form-control"
                                                type="text"
                                                onChange={(e) => this.onChangeVideo(e.target.value)}
                                                value={this.state.video_url || 0}
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label className="col-sm-2 control-label">題號</label>
                                        <div className="col-sm-10">

                                            <select className="selectpicker form-control" placeholder="Select" value={this.state.select_json + 1} onChange={(e) => this.onChangeQuest(e.target.value)}>
                                                {
                                                    quest_title.map((d, i) => {
                                                        return (
                                                            <option key={i} >{i + 1}</option>
                                                        )
                                                    })
                                                }
                                            </select>


                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <div className="row">
                                            <label className="col-sm-2 control-label">題目及選項</label>
                                            <div className="col-sm-8">

                                                <input className="form-control" type="text" value={quest_title[this.state.select_json] || 0} onChange={(e) => this.onChangeQuest_title(e.target.value)} />

                                            </div>

                                        </div>
                                        <div className="row">
                                            <div className="col-sm-6 col-sm-offset-2">
                                                <input id='se1' className="form-control" onChange={(e) => this.onChangeQuest_s(e.target)} type="text" value={qut[0] || 0} />
                                                <input id='se2' className="form-control" onChange={(e) => this.onChangeQuest_s(e.target)} type="text" value={qut[1] || 0} />
                                                <input id='se3' className="form-control" onChange={(e) => this.onChangeQuest_s(e.target)} type="text" value={qut[2] || 0} />
                                                <input id='se4' className="form-control" onChange={(e) => this.onChangeQuest_s(e.target)} type="text" value={qut[3] || 0} />

                                            </div>
                                        </div>
                                        <div className="row">
                                            <label className="col-sm-2 control-label">答案</label>

                                            <div className="col-sm-2">

                                                <select className="selectpicker form-control" placeholder="Select" value={quest_ans[this.state.select_json]} onChange={(e) => this.onChangeAns(e.target.value)}>
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="form-group">

                                        <div className="col-sm-10">
                                            <button className="btn btn-info" type="button" onClick={(e) => this.onClickAdd()}>新增提號</button>
                                            <button className="btn btn-danger" type="button" onClick={(e) => this.onClickremove()}>刪除提號</button>
                                            <button className="btn btn-success" type="submit" onClick={(e) => this.onClickSave()}>Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            )
        } else {
            return (
                <div></div>
            )
        }
    }

}
