import React, { Component } from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { NotificationManager } from 'react-notifications';


const area2 = [
    '第一校區',
    '楠梓校區',
    '旗津校區',
    '建工校區',
    '燕巢校區'

];
const area = {
    '第一校區': '第一校區',
    '楠梓校區': '楠梓校區',
    '旗津校區': '旗津校區',
    '建工校區': '建工校區',
    '燕巢校區': '燕巢校區'
};
const auth = {
    '管理者': '管理者',
    '協助者': '協助者'
};
const auth2 = [
    '管理者', '協助者'
];
export default class back_auth extends Component {
    constructor(props) {
        super(props)

        this.state = {
            auth_data: null,
            mybase: null
        };
        this.getVideo_info = this.getVideo_info.bind(this)
        this.onAddRow = this.onAddRow.bind(this)
        this.afterSaveCell = this.afterSaveCell.bind(this)
        this.onDeleteRow = this.onDeleteRow.bind(this)
    };

    componentDidMount() {
        if (this.props.user_base.mybase) {
            this.getVideo_info(this.props)
            console.log(this.props)
        } else {
            console.log('componedid  no')
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.user_base.mybase != this.props.user_base.mybase) {
            this.getVideo_info(nextProps)
            console.log("prosps hange")
        }



    }
    getVideo_info(Props) {
        const { user_base } = Props

        const rej = {
            "user_base": user_base.mybase

        }
        fetch(this.props.url + "b_get_auth",
            {
                method: 'post',
                body: JSON.stringify(rej)
            }).then(response => Promise.all([response, response.json()])).then(([response, json]) => {
                if (response.status === 200) {

                    if (json.status == "success") {


                        this.setState({ auth_data: json.data })
                        this.setState({ mybase: user_base.mybase })

                    }

                }
                else {

                }
            }).catch((error) => {
                console.log(error)
            });
    }
    render() {

        // const college = {
        //     '工學院': '工學院',
        //     '電資學院': '電資學院',
        //     '管理學院': '管理學院',
        //     '海工學院': '海工學院',
        //     '水圈學院': '水圈學院',
        //     '海事學院': '海事學院',
        // };

        const options = {
            insertBtn: this.createCustomInsertButton,
            onAddRow: this.onAddRow,
            deleteBtn: this.createCustomDeleteButton,
            onDeleteRow: this.onDeleteRow
        };
        const cellEdit = {
            mode: 'click', // click cell to edit
            afterSaveCell: this.afterSaveCell,
            beforeSaveCell: this.beforeSaveCell,
        };

        const selectRow = {
            mode: 'radio'
        };
        return (
            <div>

                <BootstrapTable selectRow={selectRow} data={this.state.auth_data} options={options} insertRow cellEdit={cellEdit} deleteRow>
                    <TableHeaderColumn editable={false} dataField='id' isKey hidden>序號</TableHeaderColumn>
                    <TableHeaderColumn dataField='mail' dataSort={true}>帳號</TableHeaderColumn>
                    <TableHeaderColumn dataField='area' filterFormatted dataFormat={this.enumFormatter} formatExtraData={area}
                        filter={{ type: 'SelectFilter', options: area }} dataSort={true} editable={{ type: 'select', options: { values: area2 } }} customInsertEditor customInsertEditor={{ getElement: this.customNameField }} >校區</TableHeaderColumn>
                    <TableHeaderColumn dataField='dep' dataSort={true}>科系</TableHeaderColumn>
                    <TableHeaderColumn dataField='auth' filterFormatted dataFormat={this.enumFormatter} formatExtraData={auth}
                        filter={{ type: 'SelectFilter', options: auth }} dataSort={true} editable={{ type: 'select', options: { values: auth2 } }} >身分</TableHeaderColumn>



                </BootstrapTable>

            </div>
        )

    }
    enumFormatter(cell, row, enumObject) {
        return enumObject[cell];
    }

    createCustomInsertButton = (onClick) => {
        return (
            <InsertButton
                btnText='新增資料'
                btnContextual='btn-info'
                className='my-custom-class'
                btnGlyphicon='glyphicon-edit'
                onClick={() => this.handleInsertButtonClick(onClick)} />
        );
    }
    createCustomDeleteButton = (onClick) => {
        return (
            <InsertButton
                btnText='刪除資料'
                btnContextual='btn-danger'
                className='my-custom-class'
                btnGlyphicon='glyphicon-edit'
                onClick={() => this.handleDeleteButtonClick(onClick)} />
        );
    }
    handleDeleteButtonClick = (onClick) => {

        onClick();
    }

    handleInsertButtonClick = (onClick) => {

        onClick();
    }

    onAddRow(row) {



        const rej = {
            "user_base": this.state.mybase,
            "mail": row.mail,
            "auth": row.auth,
            "dep": row.dep,
            "area": row.area

        }
        fetch(this.props.url + "b_set_auth",
            {
                method: 'post',
                body: JSON.stringify(rej)
            }).then(response => Promise.all([response, response.json()])).then(([response, json]) => {
                if (response.status === 200) {

                    if (json.status == "success") {


                        this.getVideo_info(this.props)

                    }

                    NotificationManager.info(json.msg);
                }
                else {

                }
            }).catch((error) => {
                console.log(error)
            });
    }
    onDeleteRow(row) {


        const rej = {
            "user_base": this.state.mybase,
            "id": row,


        }
        fetch(this.props.url + "b_delete_auth",
            {
                method: 'post',
                body: JSON.stringify(rej)
            }).then(response => Promise.all([response, response.json()])).then(([response, json]) => {
                if (response.status === 200) {

                    if (json.status == "success") {


                        this.getVideo_info(this.props)

                    }

                    NotificationManager.info(json.msg);
                }
                else {

                }
            }).catch((error) => {

                NotificationManager.warning(error, 'Close after 3000ms', 3000);
            });
    }

    afterSaveCell(row, cellName, cellValue) {
        const rej = {
            "user_base": this.state.mybase,
            "id": row.id,
            "mail": row.mail,
            "auth": row.auth,
            "dep": row.dep,
            "area": row.area

        }
        fetch(this.props.url + "b_update_auth",
            {
                method: 'post',
                body: JSON.stringify(rej)
            }).then(response => Promise.all([response, response.json()])).then(([response, json]) => {
                if (response.status === 200) {

                    if (json.status == "success") {


                        this.getVideo_info(this.props)


                    }

                    NotificationManager.info(json.msg);
                }
                else {

                }
            }).catch((error) => {
                console.log(error)
            });
    }
    customNameField = (column, attr, editorClass, ignoreEditable) => {


        return (

            <select className={`${editorClass}`} {...attr}>
                {
                    area2.map((d) => {
                        return (
                            <option key={"area" + d} >{d}</option>
                        )
                    })
                }
            </select>
        );


    }
};
