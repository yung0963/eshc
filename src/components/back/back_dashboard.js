
import React from 'react';
import Video from './video'
import Back_student from './back_student'
import Back_chart from './back_chart'
import Back_auth from './back_auth'
import Back_table from './back_table'


// const outerDashboard = () => (
//   <div>Hello, World!</div>
// );

export default class back_dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      jsondata: ""

    }

  }


  render() {
    let img_url = this.props.user_data.imageUrl ? this.props.user_data.imageUrl : ""


    if (this.props.user_data) {

      name = this.props.user_data.email

    }
    let conponet, con_title;

    if (this.props.page) {

      switch (this.props.page) {
        case "video":
          conponet = <Video user_data={this.props.userInfo} user_base={this.props.user_base} url={this.props.url} />
          con_title = "教育訓練影片管理"
          break
        case "student":
          conponet = <Back_student user_data={this.props.userInfo} user_base={this.props.user_base} url={this.props.url} />
          con_title = "學生管理"
          break
        case "chart":
          conponet = <Back_chart user_data={this.props.userInfo} user_base={this.props.user_base} url={this.props.url} />
          con_title = "圖表數據化"
          break
        case "table":
          conponet = <Back_table user_data={this.props.userInfo} user_base={this.props.user_base} url={this.props.url} />
          con_title = "統計表"
          break
        case "auth":
          conponet = <Back_auth user_data={this.props.userInfo} user_base={this.props.user_base} url={this.props.url} />
          con_title = "權限設定"
          break
      }

    }
    return (

      <div className="dashboard">
        <div className="sidebar">

          <div className="scrollable scrollbar-macosx">
            <div className="sidebar__cont">

              <div className="sidebar__menu">
                <div className="sidebar__title">使用者資訊</div>
                <div className="ss-widget">
                  <div className="ss-widget__cont">
                    <br />
                    <div className="ss-widget__row">
                      <img src={img_url} />
                    </div>
                    <br /><br />
                    <div className="ss-widget__row" style={{ color: "white" }}>
                      帳號：{name}<br /><br />



                    </div>



                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
        <div className="main">
          <div className="main__scroll scrollbar-macosx">
            <div className="main__cont">
              <div className="container-fluid half-padding">
                <div className="template template__blank">
                  <div className="row">
                    <div className="col-md-12">
                      <div className="panel panel-danger">
                        <div className="panel-heading">
                          <h3 className="panel-title">{con_title}</h3>
                        </div>
                        <div className="panel-body">


                          {conponet}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    )


  }
}